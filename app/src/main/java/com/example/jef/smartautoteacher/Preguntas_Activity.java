package com.example.jef.smartautoteacher;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.jef.smartautoteacher.Adapters.RecyclerCategoríasAdapter;
import com.example.jef.smartautoteacher.Adapters.RecyclerPreguntasAdapter;
import com.example.jef.smartautoteacher.POCOS.Categoria;
import com.example.jef.smartautoteacher.POCOS.Pregunta;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

public class Preguntas_Activity extends AppCompatActivity {


    public List<Pregunta> lstPreguntas;
    public Pregunta pregunta_nueva;
    public RecyclerView rvPreguntas;
    public RecyclerPreguntasAdapter _adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preguntas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvPreguntas=(RecyclerView) findViewById(R.id.recyclerPreguntas);


        this.setTitle("Editar preguntas");

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llenarlista();
                showInputDialog(view);

            }
        });

        llenarlista();

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(Preguntas_Activity.this);
        mLayoutManager = new LinearLayoutManager(Preguntas_Activity.this);
        rvPreguntas.setLayoutManager(mLayoutManager);
        rvPreguntas.setItemAnimator(new DefaultItemAnimator());
        _adapter = new RecyclerPreguntasAdapter(lstPreguntas, Preguntas_Activity.this);
        rvPreguntas.setAdapter(_adapter);


    }


    public void llenarlista(){

        lstPreguntas = new ArrayList<Pregunta>();

        for (int x=0; x<4; x++){
            Pregunta pregunta= new Pregunta();

            switch (x){
                case (0):
                        pregunta.Id_pregunta=1;
                        pregunta.Enunciado="Ecuación de la línea recta conociendo dos puntos:";
                        pregunta.Respuesta_Correcta=" y- y1 = m(x + x1)";
                        pregunta.Respuesta_Incorrecta= "y = mx + b";
                        pregunta.Respuesta_Incorrecta_2="(y2-y1)/(x2-x1)";

                    break;
                case (1):
                    pregunta.Id_pregunta=2;
                    pregunta.Enunciado="Ecuación de la pendiente:";
                    pregunta.Respuesta_Correcta=" (y2-y1)/(x2-x1))";
                    pregunta.Respuesta_Incorrecta= "y = mx + b";
                    pregunta.Respuesta_Incorrecta_2="y- y1 = m(x + x1)";


                    break;

                case  (2):
                    pregunta.Id_pregunta=3;
                    pregunta.Enunciado="Pendiente de una recta paralela al eje x:";
                    pregunta.Respuesta_Correcta=" Indefinida";
                    pregunta.Respuesta_Incorrecta= "1";
                    pregunta.Respuesta_Incorrecta_2="0";

                    break;

                case (3):
                    pregunta.Id_pregunta=4;
                    pregunta.Enunciado="Pendiente de una recta paralela al eje y:";
                    pregunta.Respuesta_Correcta=" 0";
                    pregunta.Respuesta_Incorrecta= "1";
                    pregunta.Respuesta_Incorrecta_2="indefinida";

                    break;
            }
            lstPreguntas.add(pregunta);

        }


    }

    protected void showInputDialog(final View view) {
        // get prompts.xml view


        LayoutInflater layoutInflater = LayoutInflater.from(Preguntas_Activity.this);
        final View promptView = layoutInflater.inflate(R.layout.input_edit_preguntas, null);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(Preguntas_Activity.this);
        alertDialogBuilder.setView(promptView);

        final EditText mTxtPregunta= (EditText) promptView.findViewById(R.id.txtEditPregunta);
        final EditText mTxtRespuestaC= (EditText) promptView.findViewById(R.id.txtEditRespuesta);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        pregunta_nueva= new Pregunta();

                        pregunta_nueva.Enunciado=  mTxtPregunta.getText().toString();
                        pregunta_nueva.Respuesta_Correcta= mTxtRespuestaC.getText().toString();

                        Snackbar.make(view, "Se añadió la pregunta correctamente", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                        lstPreguntas.add(pregunta_nueva);

                        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(Preguntas_Activity.this);
                        mLayoutManager2 = new LinearLayoutManager(Preguntas_Activity.this);
                        rvPreguntas.setLayoutManager(mLayoutManager2);
                        rvPreguntas.setItemAnimator(new DefaultItemAnimator());
                        _adapter = new RecyclerPreguntasAdapter(lstPreguntas, Preguntas_Activity.this);
                        rvPreguntas.setAdapter(_adapter);


                    }
                })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();


    }



}
