package com.example.jef.smartautoteacher.Adapters;

import android.app.AlertDialog;
import  android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.example.jef.smartautoteacher.R;

import java.util.ArrayList;
import com.example.jef.smartautoteacher.POCOS.Categoria;
import com.example.jef.smartautoteacher.SubCategorias_Activity;

/**
 * Created by jef on 28/11/2017.
 */

public class RecyclerCategoríasAdapter extends RecyclerView.Adapter<RecyclerCategoríasAdapter.ViewHolder>  {

    public ArrayList<Categoria> _listaCategoria;
    public View.OnClickListener _clicklistener;
    private Context _contexto;

    public RecyclerCategoríasAdapter(ArrayList<Categoria> lista, Context ctx) {
        this._listaCategoria = lista;
        this._contexto = ctx;
    }

    @Override
    public RecyclerCategoríasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(_contexto).inflate(R.layout.recycler_categorias, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerCategoríasAdapter.ViewHolder holder, int position) {
        Categoria al = _listaCategoria.get(position);
        holder.txtCategoria.setText(al.strCategoria);
        holder.setOnClickListeners();
    }


    public void refrechData(ArrayList<Categoria> lista) {
        _listaCategoria.clear();
        _listaCategoria.addAll(lista);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return _listaCategoria.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        public TextView txtCategoria;
        public ImageButton btnDeleteCate;
        public ImageButton btnEditCate;
        public CardView cvCategoria;


        public ViewHolder(View v) {
            super(v);
            context = v.getContext();

            btnDeleteCate = (ImageButton) v.findViewById(R.id.btnEliminarCategoria);
            btnEditCate = (ImageButton) v.findViewById(R.id.btnEditCategoria);
            txtCategoria = (TextView) v.findViewById(R.id.txtCategoria);
            cvCategoria=(CardView) v.findViewById(R.id.CardCategoria);
        }

        void setOnClickListeners() {
            btnEditCate.setOnClickListener(this);
            btnDeleteCate.setOnClickListener(this);
            cvCategoria.setOnClickListener(this);

        }

        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.btnEliminarCategoria:

                    new AlertDialog.Builder(context).setTitle("Alerta").setMessage("¿Desea eliminar la categoría "+txtCategoria.getText()+"?")
                            .setIcon(R.mipmap.ic_delete)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    Snackbar.make(view, "Eliminó " + txtCategoria.getText()  +" correctamente", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();

                                    cvCategoria.setVisibility(View.GONE);

                                }
                            })
                            .setNegativeButton("CANCELAR", null).show();


                    break;
                case R.id.btnEditCategoria:
                String cate= txtCategoria.getText().toString();


                     showInputDialog(view);



                    break;

                    default:

                        context.startActivity(new Intent(context, SubCategorias_Activity.class));

                        break;


            }
        }


        protected void showInputDialog(final View view) {
            // get prompts.xml view


            final String cate= String.valueOf( txtCategoria.getText());

            LayoutInflater layoutInflater = LayoutInflater.from(context);
            final View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            alertDialogBuilder.setView(promptView);

            final EditText mTxtCURP = (EditText) promptView.findViewById(R.id.txtCURP);
            mTxtCURP.setText(txtCategoria.getText().toString());
            // setup a dialog window
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                       txtCategoria.setText(mTxtCURP.getText().toString());

                            Snackbar.make(view, "La categoría " + cate +" cambió a "+ txtCategoria.getText(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                    })
                    .setNegativeButton("CANCELAR",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create an alert dialog
            android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();

        }

    }





}