package com.example.jef.smartautoteacher;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.jef.smartautoteacher.Adapters.RecyclerCategoríasAdapter;
import com.example.jef.smartautoteacher.Adapters.RecyclerSubcategoriasAdapter;
import com.example.jef.smartautoteacher.POCOS.Categoria;
import com.example.jef.smartautoteacher.POCOS.Subcategoria;

import java.util.ArrayList;

public class SubCategorias_Activity extends AppCompatActivity {

    public RecyclerView rvSubcategoria;
    public ArrayList<Subcategoria> lstSubcategoria;
    public RecyclerSubcategoriasAdapter _adapter;
    public ImageButton btnEliminar;
    public Subcategoria subcategoria_nueva= new Subcategoria();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_categorias);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        setTitle("Subcategorías");
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(SubCategorias_Activity.this);

        rvSubcategoria=(RecyclerView) findViewById(R.id.rvsubcategoria);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llenarlista();
                Subcategoria cate = new Subcategoria();
                showInputDialog(view);
            }
        });

        llenarlista();

        mLayoutManager = new LinearLayoutManager(SubCategorias_Activity.this);
        rvSubcategoria.setLayoutManager(mLayoutManager);
        rvSubcategoria.setItemAnimator(new DefaultItemAnimator());
        _adapter = new RecyclerSubcategoriasAdapter(lstSubcategoria, SubCategorias_Activity.this);
        rvSubcategoria.setAdapter(_adapter);

    }

    public void llenarlista(){

        lstSubcategoria= new ArrayList<Subcategoria>();

        for (int x=0; x<3; x++){
            Subcategoria subcategoria= new Subcategoria();

            switch (x){
                case (0):subcategoria.strSubcategoria="Línea Recta";
                    break;
                case (1): subcategoria.strSubcategoria="Circunferencia";
                    break;

                case  (2):  subcategoria.strSubcategoria="Parábola";
                    break;

            }
            lstSubcategoria.add(subcategoria);

        }


    }


    protected void showInputDialog(final View view) {
        // get prompts.xml view


        LayoutInflater layoutInflater = LayoutInflater.from(SubCategorias_Activity.this);
        final View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(SubCategorias_Activity.this);
        alertDialogBuilder.setView(promptView);

        final EditText mTxtCURP = (EditText) promptView.findViewById(R.id.txtCURP);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        subcategoria_nueva.strSubcategoria=  mTxtCURP.getText().toString();
                        lstSubcategoria.add(subcategoria_nueva);

                        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(SubCategorias_Activity.this);
                        mLayoutManager2 = new LinearLayoutManager(SubCategorias_Activity.this);
                        rvSubcategoria.setLayoutManager(mLayoutManager2);
                        rvSubcategoria.setItemAnimator(new DefaultItemAnimator());
                        _adapter = new RecyclerSubcategoriasAdapter(lstSubcategoria, SubCategorias_Activity.this);
                        rvSubcategoria.setAdapter(_adapter);


                        Snackbar.make(view, "Se creó la subcategoría " + subcategoria_nueva.strSubcategoria +" correctamente", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                    }
                })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();


    }


}
