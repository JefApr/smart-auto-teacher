package com.example.jef.smartautoteacher;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.jef.smartautoteacher.Adapters.RecyclerCategoríasAdapter;
import com.example.jef.smartautoteacher.POCOS.Categoria;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public RecyclerView rvCategoria;
    public ArrayList<Categoria> lstCategoria;
    public RecyclerCategoríasAdapter _adapter;
    public ImageButton btnEliminar;
    public Categoria categoria_nueva= new Categoria();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        rvCategoria=(RecyclerView) findViewById(R.id.rvcategoria);


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                llenarlista();
                Categoria cate = new Categoria();
                showInputDialog(view);


            }
        });

        llenarlista();



         mLayoutManager = new LinearLayoutManager(MainActivity.this);
        rvCategoria.setLayoutManager(mLayoutManager);
        rvCategoria.setItemAnimator(new DefaultItemAnimator());
        _adapter = new RecyclerCategoríasAdapter(lstCategoria, MainActivity.this);
        rvCategoria.setAdapter(_adapter);


        rvCategoria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Snackbar.make(view, "working " , Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void llenarlista(){

        lstCategoria= new ArrayList<Categoria>();

        for (int x=0; x<4; x++){
            Categoria categoria= new Categoria();

            switch (x){
                case (0):categoria.strCategoria="Álgebra";
                    break;
                case (1): categoria.strCategoria="Geometría";
                    break;

                case  (2):  categoria.strCategoria="Fundamentos...";
                    break;

                case (3):  categoria.strCategoria= "Algoritmia";
                    break;
            }
            lstCategoria.add(categoria);

        }


    }

    protected void showInputDialog(final View view) {
        // get prompts.xml view


        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        final View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText mTxtCURP = (EditText) promptView.findViewById(R.id.txtCURP);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    categoria_nueva.strCategoria=  mTxtCURP.getText().toString();

                        Snackbar.make(view, "Se creó la categoría " + categoria_nueva.strCategoria +" correctamente", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();

                        lstCategoria.add(categoria_nueva);

                        LinearLayoutManager mLayoutManager2 = new LinearLayoutManager(MainActivity.this);
                        mLayoutManager2 = new LinearLayoutManager(MainActivity.this);
                        rvCategoria.setLayoutManager(mLayoutManager2);
                        rvCategoria.setItemAnimator(new DefaultItemAnimator());
                        _adapter = new RecyclerCategoríasAdapter(lstCategoria, MainActivity.this);
                        rvCategoria.setAdapter(_adapter);


                    }
                })
                .setNegativeButton("CANCELAR",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
        alert.show();


    }


}
