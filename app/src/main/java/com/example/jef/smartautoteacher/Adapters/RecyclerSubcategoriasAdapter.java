package com.example.jef.smartautoteacher.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.jef.smartautoteacher.Juegos_Activity;
import com.example.jef.smartautoteacher.POCOS.Subcategoria;
import com.example.jef.smartautoteacher.Preguntas_Activity;
import com.example.jef.smartautoteacher.R;
import com.example.jef.smartautoteacher.SubCategorias_Activity;

import java.util.ArrayList;

/**
 * Created by jef on 30/11/2017.
 */

public class RecyclerSubcategoriasAdapter extends RecyclerView.Adapter<RecyclerSubcategoriasAdapter.ViewHolder>{


    public ArrayList<Subcategoria> _listaSubcategoria;
    public View.OnClickListener _clicklistener;
    private Context _contexto;

    public RecyclerSubcategoriasAdapter(ArrayList<Subcategoria> lista, Context ctx) {
        this._listaSubcategoria = lista;
        this._contexto = ctx;
    }

    @Override
    public RecyclerSubcategoriasAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(_contexto).inflate(R.layout.recycler_subcategorias, parent, false);
        ViewHolder vh= new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(RecyclerSubcategoriasAdapter.ViewHolder holder, int position) {
        Subcategoria subcategoria= _listaSubcategoria.get(position);
        holder.txtSubcategoria.setText(subcategoria.strSubcategoria);
        holder.setOnClickListeners();
    }

    @Override
    public int getItemCount() {
        return _listaSubcategoria.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        public TextView txtSubcategoria;
        public ImageButton btnDeleteSubcate;
        public ImageButton btnEditSubcate;
        public ImageButton btnSettings;
        public CardView cvSubcategoria;


        public ViewHolder(View v) {
            super(v);
            context = v.getContext();

            btnDeleteSubcate = (ImageButton) v.findViewById(R.id.btnEliminarSubcategoria);
            btnEditSubcate = (ImageButton) v.findViewById(R.id.btnEditSubcategoria);
            txtSubcategoria = (TextView) v.findViewById(R.id.txtSubcategoria);
            cvSubcategoria=(CardView) v.findViewById(R.id.CardSubcategoria);
            btnSettings=(ImageButton) v.findViewById(R.id.btnSettings);
        }

        void setOnClickListeners() {
            btnEditSubcate.setOnClickListener(this);
            btnDeleteSubcate.setOnClickListener(this);
            cvSubcategoria.setOnClickListener(this);
            btnSettings.setOnClickListener(this);

        }

        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.btnEliminarSubcategoria:

                    new AlertDialog.Builder(context).setTitle("Alerta").setMessage("¿Desea eliminar la categoría "+txtSubcategoria.getText()+"?")
                            .setIcon(R.mipmap.ic_delete)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    Snackbar.make(view, "Eliminó " + txtSubcategoria.getText()  +" correctamente", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();

                                    cvSubcategoria.setVisibility(View.GONE);

                                }
                            })
                            .setNegativeButton("CANCELAR", null).show();


                    break;
                case R.id.btnEditSubcategoria:
                    String cate= txtSubcategoria.getText().toString();


                    showInputDialog(view);



                    break;

                case R.id.btnSettings:
                    context.startActivity(new Intent(context, Preguntas_Activity.class));

                    break;

                default:
                    context.startActivity(new Intent(context, Juegos_Activity.class));

                    break;

            }
        }


        protected void showInputDialog(final View view) {
            // get prompts.xml view


            final String cate= String.valueOf( txtSubcategoria.getText());

            LayoutInflater layoutInflater = LayoutInflater.from(context);
            final View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            alertDialogBuilder.setView(promptView);

            final EditText mTxtCURP = (EditText) promptView.findViewById(R.id.txtCURP);
            mTxtCURP.setText(txtSubcategoria.getText().toString());

            // setup a dialog window
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            txtSubcategoria.setText(mTxtCURP.getText().toString());

                            Snackbar.make(view, "La categoría " + cate +" cambió a "+ txtSubcategoria.getText(), Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                    })
                    .setNegativeButton("CANCELAR",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create an alert dialog
            android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();

        }

    }


}
