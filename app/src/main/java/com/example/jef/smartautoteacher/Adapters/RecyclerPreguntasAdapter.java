package com.example.jef.smartautoteacher.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.jef.smartautoteacher.POCOS.Pregunta;
import com.example.jef.smartautoteacher.POCOS.Subcategoria;
import com.example.jef.smartautoteacher.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jef on 3/12/2017.
 */

public class RecyclerPreguntasAdapter extends RecyclerView.Adapter<RecyclerPreguntasAdapter.ViewHolder> {

    public List<Pregunta> lstPreguntas;
    public View.OnClickListener _clicklistener;
    private Context _contexto;

    public RecyclerPreguntasAdapter(List<Pregunta> lista, Context ctx) {
        this.lstPreguntas = lista;
        this._contexto = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(_contexto).inflate(R.layout.recycler_preguntas, parent, false);
        RecyclerPreguntasAdapter.ViewHolder vh= new RecyclerPreguntasAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pregunta pregunta= lstPreguntas.get(position);
        holder.txtPregunta.setText(pregunta.Enunciado);
        holder.txtGonePregunta.setText(pregunta.Enunciado);
        holder.txtGoneRespuesta.setText(pregunta.Respuesta_Correcta);
        holder.setOnClickListeners();
    }

    @Override
    public int getItemCount() {
        return lstPreguntas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        Context context;
        public TextView txtPregunta;
        public ImageButton btnDeletePregunta;
        public ImageButton btnEditPregunta;
        public CardView cvPregunta;
        public TextView txtGonePregunta;
        public TextView txtGoneRespuesta;


        public ViewHolder(View v) {
            super(v);
            context = v.getContext();

            btnDeletePregunta = (ImageButton) v.findViewById(R.id.btnEliminarPregunta);
            btnEditPregunta = (ImageButton) v.findViewById(R.id.btnEditPregunta);
            txtPregunta = (TextView) v.findViewById(R.id.txtPregunta);
            cvPregunta=(CardView) v.findViewById(R.id.CardPregunta);
            txtGonePregunta=(TextView) v.findViewById(R.id.txtGonePregunta);
            txtGoneRespuesta=(TextView) v.findViewById(R.id.txtGoneRespuesta);

        }

        void setOnClickListeners() {
            btnEditPregunta.setOnClickListener(this);
            btnDeletePregunta.setOnClickListener(this);
            cvPregunta.setOnClickListener(this);

        }

        @Override
        public void onClick(final View view) {
            switch (view.getId()) {
                case R.id.btnEliminarPregunta:

                    new AlertDialog.Builder(context).setTitle("Alerta").setMessage("¿Desea eliminar la pregunta?")
                            .setIcon(R.mipmap.ic_delete)
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    Snackbar.make(view, " Se eliminó la pregunta correctamente", Snackbar.LENGTH_LONG)
                                            .setAction("Action", null).show();

                                    cvPregunta.setVisibility(View.GONE);

                                }
                            })
                            .setNegativeButton("CANCELAR", null).show();


                    break;
                case R.id.btnEditPregunta:
                    String cate= txtPregunta.getText().toString();


                    showInputDialog(view);



                    break;


            }
        }


        protected void showInputDialog(final View view) {
            // get prompts.xml view


            final String cate= String.valueOf( txtPregunta.getText());

            LayoutInflater layoutInflater = LayoutInflater.from(context);
            final View promptView = layoutInflater.inflate(R.layout.input_edit_preguntas, null);
            android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
            alertDialogBuilder.setView(promptView);

            final EditText mTxtPregunta = (EditText) promptView.findViewById(R.id.txtEditPregunta);
            final EditText mTxtRespuesta=(EditText) promptView.findViewById(R.id.txtEditRespuesta);

            mTxtPregunta.setText(txtGonePregunta.getText().toString());
            mTxtRespuesta.setText(txtGoneRespuesta.getText().toString());

            // setup a dialog window
            alertDialogBuilder.setCancelable(false)
                    .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            txtPregunta.setText(mTxtPregunta.getText().toString());
                            txtGonePregunta.setText(mTxtPregunta.getText().toString());
                            txtGoneRespuesta.setText(mTxtRespuesta.getText().toString());

                            Snackbar.make(view, "Se editó la pregunta correctamente", Snackbar.LENGTH_LONG)
                                    .setAction("Action", null).show();

                        }
                    })
                    .setNegativeButton("CANCELAR",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create an alert dialog
            android.support.v7.app.AlertDialog alert = alertDialogBuilder.create();
            alert.show();

        }

    }




}
